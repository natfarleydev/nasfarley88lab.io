module.exports = {
  title: "Nathanael Farley",
  description: "Personal site of Nathanael Farley.",
  dest: "public",
  themeConfig: {
    sidebar: ["/", "writingsamples/writingsample_lhcbpr_nathanael_farley.md"]
  }
};
